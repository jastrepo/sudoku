#include "Field.h"

void Field::init(const int elements[9][9]) {
  set<int> base;
  for (int i = 0; i < 9; i++) {
    base.insert(i + 1);
  }
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      columns[i][j] = new FieldElement{elements[i][j], base, i, j};
    }
  }
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      rows[i][j] = columns[j][i];
    }
  }
  for (int i = 0; i < 9; i++) {
    int index = 0;
    for (int j = (i / 3) * 3; j < (i / 3 + 1) * 3; j++) {
      for (int k = (i % 3) * 3; k < (i % 3 + 1) * 3; k++) {
        squares[i][index++] = columns[j][k];
      }
    }
  }
  deleteAllImpossible();
  isParadox();
  isAllSolved();
}

bool Field::deleteImpossibleInSelection(FieldElement *selection[9], int i) {
  if (!finalized) {
    int elementValue = selection[i]->value;
    if (elementValue != 0) {
      selection[i]->possible.clear();
      for (int j = 0; j < 9; j++) {
        if (i == j) continue;
        selection[j]->possible.erase(elementValue);
      }
    }
    return true;
  }
  return false;
}

bool Field::deleteImpossibleForElement(FieldElement *element) {
  if (!finalized) {
    int i = element->i, j = element->j;
    return deleteImpossibleInSelection(columns[i], j) && deleteImpossibleInSelection(rows[j], i) &&
           deleteImpossibleInSelection(squares[(i / 3) * 3 + (j / 3)], (i - (i / 3) * 3) * 3 + (j - (j / 3) * 3)) &&
           check1(columns[i]) && check1(rows[j]) && check1(squares[(i / 3) * 3 + (j / 3)]);
  }
  return false;
}

bool Field::deleteAllImpossible() {
  if (!finalized) {
    for (const auto &column:columns) {
      for (auto i:column) {
        if (!deleteImpossibleForElement(i)) {
          return false;
        }
      }
    }
    return true;
  }
  return false;
}

bool Field::check0(FieldElement *selection[9]) {
  if (!finalized) {
    vector<bool> possible(9, false);
    for (int i = 0; i < 9; i++) {
      int value = selection[i]->value - 1;
      if (value != -1) {
        if (possible[value]) {
          paradox = finalized = true;
          return false;
        }
        possible[value] = true;
      }
    }
    return true;
  }
  return false;
}

bool Field::check0All() {
  if (!finalized) {
    for (auto &column:columns) {
      if (!check0(column)) {
        return false;
      }
    }
    for (auto &row:rows) {
      if (!check0(row)) {
        return false;
      }
    }
    for (auto &square:squares) {
      if (!check0(square)) {
        return false;
      }
    }
    return true;
  }
  return false;
}

bool Field::check1(FieldElement *selection[9]) {
  if (!finalized) {
    for (int i = 0; i < 9; i++) {
      auto element = selection[i];
      if (element->possible.empty() && element->value == 0) {
        paradox = finalized = true;
        return false;
      }
    }
    return true;
  }
  return false;
}

bool Field::check1All() {
  if (!finalized) {
    for (auto &column:columns) {
      if (!check1(column)) {
        return false;
      }
    }
    return true;
  }
  return false;
}

int Field::fill1() {
  if (!finalized) {
    int changed = 0;
    for (const auto &column : columns) {
      for (auto element : column) {
        if (element->possible.size() == 1) {
          element->value = *(element->possible.cbegin());
          if (!deleteImpossibleForElement(element)) {
            return -1;
          }
          changed++;
        }
      }
    }
    isAllSolved();
    return changed;
  }
  return -1;
}

bool Field::check2(FieldElement *selection[9]) {
  if (!finalized) {
    pair<int, int> possible[9];
    for (int i = 0; i < 9; i++) {
      int value = selection[i]->value - 1;
      if (value != -1) {
        possible[value] = {-1, i};
      } else {
        for (int j:selection[i]->possible) {
          if (possible[j - 1].first != -1) {
            possible[j - 1] = {possible[j - 1].first + 1, i};
          }
        }
      }
    }
    for (const auto &i:possible) {
      if (i.first == 0) {
        paradox = finalized = true;
        return false;
      }
    }
    return true;
  }
  return false;
}

bool Field::check2All() {
  if (!finalized) {
    for (auto &column:columns) {
      if (!check2(column)) {
        return false;
      }
    }
    for (auto &row:rows) {
      if (!check2(row)) {
        return false;
      }
    }
    for (auto &square:squares) {
      if (!check2(square)) {
        return false;
      }
    }
    return true;
  }
  return false;
}

int Field::fill2(FieldElement *selection[9]) {
  if (!finalized) {
    pair<int, int> possible[9];
    int changed = 0;
    for (int i = 0; i < 9; i++) {
      if (selection[i]->value != 0) {
        possible[selection[i]->value - 1] = {-1, i};
      }
      for (int j:selection[i]->possible) {
        if (possible[j - 1].first != -1) {
          possible[j - 1] = {possible[j - 1].first + 1, i};
        }
      }
    }
    for (int i = 0; i < 9; i++) {
      const auto &j = possible[i];
      if (j.first == 0) {
        paradox = finalized = true;
        return -1;
      }
      if (j.first == 1) {
        selection[j.second]->value = i + 1;
        if (!deleteImpossibleForElement(selection[j.second])) {
          return -1;
        }
        changed++;
      }
    }
    isAllSolved();
    return changed;
  }
  return -1;
}

int Field::fill2All() {
  if (!finalized) {
    int summaryChanged = 0;
    for (auto &column:columns) {
      int changed = fill2(column);
      if (changed == -1) {
        return -1;
      } else {
        summaryChanged += changed;
      }
    }
    for (auto &row:rows) {
      int changed = fill2(row);
      if (changed == -1) {
        return -1;
      } else {
        summaryChanged += changed;
      }
    }
    for (auto &square:squares) {
      int changed = fill2(square);
      if (changed == -1) {
        return -1;
      } else {
        summaryChanged += changed;
      }
    }
    return summaryChanged;
  }
  return -1;
}

bool Field::checkAll() {
  return !finalized && check0All() && check1All() && check2All();
}

bool Field::isSolved(FieldElement *selection[9]) {
  if (!paradox) {
    if (!solved) {
      vector<bool> possible(9, false);
      for (int i = 0; i < 9; i++) {
        int value = selection[i]->value - 1;
        if (value != -1) {
          possible[value] = true;
        }
      }
      for (bool i:possible) {
        if (!i) {
          return false;
        }
      }
    }
    return true;
  }
  return false;
}

Field::Field(const int elements[9][9]) {
  init(elements);
}

Field::Field(vector<vector<int>> elements) {
  int elementsArray[9][9];
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      elementsArray[i][j] = elements[i][j];
    }
  }
  init(elementsArray);
}

Field::Field(string str) {
  int elements[9][9];
  int index = 0;
  for (auto &i:elements) {
    for (int &j:i) {
      int c = str[index++] - '0';
      if (c < 0 || c > 9) {
        c = 0;
      }
      j = c;
    }
  }
  init(elements);
}

Field::Field(const Field &second) {
  int elements[9][9];
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      elements[i][j] = second.columns[i][j]->value;
    }
  }
  init(elements);
}

Field::~Field() {
  for (const auto &column:columns) {
    for (auto i:column) {
      delete i;
    }
  }
}

int Field::fillAll() {
  if (!finalized) {
    int changed = 0, changedNow = 1;
    while (changedNow > 0) {
      int changed1 = fill1();
      int changed2 = fill2All();
      if (changed1 == -1 || changed2 == -1) {
        return -1;
      }
      changedNow = changed1 + changed2;
      changed += changedNow;
    }
    return changed;
  }
  return -1;
}

void Field::print() {
  for (const auto &column:columns) {
    cout << "|";
    for (auto i:column) {
      cout << char(i->value > 0 ? i->value + '0' : ' ') << "|";
    }
    cout << endl;
  }
  if (solved) {
    cout << "solved" << endl;
  }
  if (paradox) {
    cout << "paradox" << endl;
  }

}

bool Field::isAllSolved() {
  if (!paradox) {
    if (!solved) {
      for (auto &column:columns) {
        if (!isSolved(column)) {
          return false;
        }
      }
      for (auto &row:rows) {
        if (!isSolved(row)) {
          return false;
        }
      }
      for (auto &square:squares) {
        if (!isSolved(square)) {
          return false;
        }
      }
      solved = finalized = true;
    }
    return true;
  }
  return false;
}

bool Field::isParadox() {
  return (finalized ? checkAll() : false), paradox;
}

vector<Field *> Field::getMinPossibleVariants() {
  if (!finalized) {
    auto elements = columns[0][0];
    unsigned long min = 1000;
    for (const auto &column:columns) {
      for (auto i:column) {
        if (i->value == 0 && i->possible.size() < min) {
          min = i->possible.size();
          elements = i;
        }
      }
    }
    if (min < 1000) {
      vector<Field *> result;
      for (int i:elements->possible) {
        elements->value = i;
        result.push_back(new Field(*this));
      }
      elements->value = 0;
      return result;
    }
  }
  return vector<Field *>();
}

bool Field::operator==(const Field &second) {
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      if (columns[i][j]->value != second.columns[i][j]->value) {
        return false;
      }
    }
  }
  return true;
}

bool Field::similar(const Field &second) {
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      if (columns[i][j]->value != 0 && second.columns[i][j]->value != 0 &&
          columns[i][j]->value != second.columns[i][j]->value) {
        return false;
      }
    }
  }
  return true;
}

int **Field::getData() {
  int **ans = new int *[9];
  for (int i = 0; i < 9; i++) {
    ans[i] = new int[9];
    for (int j = 0; j < 9; j++) {
      ans[i][j] = columns[i][j]->value;
    }
  }
  return ans;
}
