//
// Created by nikita on 01.11.18.
//

#include "Solver.h"

void Solver::addToList(list<Field *> &fields, Field *newField) {
  for (auto field:fields) {
    if (field->similar(*newField)) {
      return;
    }
  }
  fields.push_back(newField);
}

void Solver::findSolutions(Field *f) {
  if (!solved) {
    list<Field *> answersList, fields;
    fields.push_back(f);
    while (!fields.empty()) {
      list<Field *> newFields;
      for (auto field:fields) {
        field->fillAll();
        if (field->isAllSolved()) {
          addToList(answersList, field);
        } else {
          if (!field->isParadox()) {
            auto variants = field->getMinPossibleVariants();
            for (const auto &variant:variants) {
              newFields.push_back(variant);
            }
          }
          delete field;
        }
      }
      fields = newFields;
    }
    answers = {answersList.cbegin(), answersList.cend()};
    solved = true;
  }
}

Solver::Solver(Field problem) {
  findSolutions(new Field(problem));
}

Solver::~Solver() {
  for (const auto &answer:answers) {
    delete answer;
  }
}

Field *Solver::getOneSolution() {
  return answers[0];
}

vector<Field *> Solver::getAllSolutions() {
  return answers;
}

int Solver::getNumOfSolutions() {
  return answers.size();
}

