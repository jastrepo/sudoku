//
// Created by nikita on 29.10.18.
//
#ifndef SUDOKU_FIELD_H
#define SUDOKU_FIELD_H

#include <vector>
#include <set>
#include <iostream>

using namespace std;
struct FieldElement {
  int value;
  set<int> possible;
  int i, j;
};

class Field {
  FieldElement *columns[9][9];
  FieldElement *rows[9][9];
  FieldElement *squares[9][9];
  bool paradox = false, solved = false, finalized = false;

  void init(const int elements[9][9]);

  bool deleteImpossibleInSelection(FieldElement *selection[9], int i);

  bool deleteImpossibleForElement(FieldElement *element);

  bool deleteAllImpossible();

  bool check0(FieldElement *selection[9]);

  bool check0All();

  bool check1(FieldElement *selection[9]);

  bool check1All();

  int fill1();

  bool check2(FieldElement *selection[9]);

  bool check2All();

  int fill2(FieldElement *selection[9]);

  int fill2All();

  bool checkAll();

  bool isSolved(FieldElement *selection[9]);

public:
  explicit Field(const int elements[9][9]);

  explicit Field(vector<vector<int>> elements);

  explicit Field(string str);

  Field(const Field &second);

  ~Field();

  int fillAll();

  void print();

  bool isAllSolved();

  bool isParadox();

  vector<Field *> getMinPossibleVariants();

  bool operator==(const Field &second);

  bool similar(const Field &second);

  int **getData();
};

#endif //SUDOKU_FIELD_H