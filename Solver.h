//
// Created by nikita on 01.11.18.
//

#ifndef SUDOKU_SOLUTOR_H
#define SUDOKU_SOLUTOR_H

#include <list>
#include "Field.h"

class Solver {
  bool solved = false;
  vector<Field *> answers;

  void addToList(list<Field *> &fields, Field *newField);

  void findSolutions(Field *f);

public:
  explicit Solver(Field problem);

  ~Solver();

  vector<Field *> getAllSolutions();

  Field *getOneSolution();

  int getNumOfSolutions();
};


#endif //SUDOKU_SOLUTOR_H
